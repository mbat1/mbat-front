import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'euroCurrency',
})
export class EuroCurrencyPipe implements PipeTransform {
  transform(value: number): string {
    const formattedValue = value.toLocaleString('fr-FR', {
      style: 'currency',
      currency: 'EUR',
      minimumFractionDigits: 0,
      maximumFractionDigits: 2
    });
    return formattedValue.replace('.', ',');
  }
}
