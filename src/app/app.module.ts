import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { AuthInterceptor } from './core/helpers/auth.interceptor';
import { SynthesisComponent } from './synthesis/synthesis.component';
import { SharedModule } from './shared/shared.module';
import { TrackingComponent } from './tracking/tracking.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { EuroCurrencyPipe } from './core/pipes/euro-currency.pipe';
import { TrackingIncomeTableComponent } from './tracking/tracking-income-table/tracking-income-table.component';

@NgModule({
  declarations: [
    AppComponent,
    SynthesisComponent,
    TrackingComponent,
    ConfigurationComponent,
    EuroCurrencyPipe,
    TrackingIncomeTableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CoreModule,
    SharedModule,
    ToastrModule.forRoot({
      timeOut: 15000, // 15 seconds
      progressBar: true,
      positionClass:'toast-bottom-center',
      maxOpened: 3,
      autoDismiss: true,
      preventDuplicates: true
    }),
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
