import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './core/services/auth.service';
import { TokenStorageService } from './core/services/token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'MBAT';
  panelOpenState = false;
  isExpanded: boolean = false;
  isLogged: boolean = false;
  username? = '';

  trackingList = [
    { year: 2023, month: 'janvier' },
    { year: 2022, month: 'février' },
  ];

  constructor(
    private router: Router,
    private auth: AuthService,
    private token: TokenStorageService,
  ) {}

  ngOnInit() {
    this.router.events.subscribe((event: any) => {
      if (event.constructor.name === 'NavigationEnd') {
        this.isLogged = this.token.isLoggedIn();
        this.username = this.token.getUser().username;
      }
    });
  }

  logout() {
    this.auth
      .logout()
      .pipe()
      .subscribe({
        complete: () => this.router.navigate(['/login']),
      });
  }

  isActiveRoute(route: string): boolean {
    return this.router.url.includes(route);
  }

  redirectToLastYear() {
    this.router.navigate(['/tracking', this.trackingList[0].year, this.trackingList[0].month])
  }
}
