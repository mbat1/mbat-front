import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { TrackingIncomeTableComponent } from './tracking-income-table/tracking-income-table.component';

@Component({
  selector: 'app-tracking',
  templateUrl: './tracking.component.html',
  styleUrls: ['./tracking.component.scss'],
})
export class TrackingComponent implements OnInit {
  @ViewChild(TrackingIncomeTableComponent)
  trackingChildComponent!: TrackingIncomeTableComponent;

  months: string[] = [
    'Janvier',
    'Février',
    'Mars',
    'Avril',
    'Mai',
    'Juin',
    'Juillet',
    'Août',
    'Septembre',
    'Octobre',
    'Novembre',
    'Décembre',
  ];
  activeMonth: string | null = null;

  constructor(private router: Router) {}

  ngOnInit(): void {
    console.log(this.router);
    this.router.events.subscribe(() => {
      let url = this.router.url.slice(15);
      let decodedMonth = decodeURIComponent(url);
      this.activeMonth =
        decodedMonth.charAt(0).toUpperCase() + decodedMonth.slice(1);
    });
  }
  addNewIncome() {
    this.trackingChildComponent.addRow();
  }

  addNewOperation() {}
}
