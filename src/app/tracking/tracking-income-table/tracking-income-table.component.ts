import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

const USER_DATA = [
  {
    id: 1,
    date: '2023-04-01',
    label: 'Salaire',
    amount: 1600.51,
  },
  {
    id: 2,
    date: '2023-04-03',
    label: 'Virement',
    amount: 200,
  },
  {
    id: 3,
    date: '2023-04-04',
    label: 'Prime',
    amount: 921.42,
  },
  {
    id: 4,
    date: '2023-04-06',
    label: 'APL',
    amount: 241.3,
  },
];

const COLUMNS_SCHEMA = [
  {
    key: 'date',
    type: 'date',
    label: 'Date',
  },
  {
    key: 'label',
    type: 'text',
    label: 'Intitulé',
  },
  {
    key: 'amount',
    type: 'number',
    label: 'Montant',
  },
  {
    key: 'isEdit',
    type: 'isEdit',
    label: '',
  },
];

@Component({
  selector: 'app-tracking-income-table',
  templateUrl: './tracking-income-table.component.html',
  styleUrls: ['./tracking-income-table.component.scss'],
})
export class TrackingIncomeTableComponent {
  displayedColumns: string[] = COLUMNS_SCHEMA.map((col) => col.key);
  dataSource = USER_DATA;
  columnsSchema: any = COLUMNS_SCHEMA;
  showTotal: boolean = false;

  constructor(public dialog: MatDialog) {}

  addRow() {
    const newRow = {
      id: Date.now(),
      date: '',
      label: '',
      amount: 0,
      isEdit: true,
    };
    this.dataSource = [newRow, ...this.dataSource];
  }

  removeRow(id: any) {
    this.dataSource = this.dataSource.filter((u) => u.id !== id);
  }

  saveIncome(element: any) {
    element.isEdit = !element.isEdit;
    element.amount = +element.amount;
  }

  removeSelectedRows() {
    // this.dialog
    //   .open(ConfirmDialogComponent)
    //   .afterClosed()
    //   .subscribe((confirm) => {
    //     if (confirm) {
    this.dataSource = this.dataSource.filter((u: any) => !u.isSelected);
    // }
    // });
  }

  getTotalCost() {
    return this.dataSource
      .map((t) => +t.amount)
      .reduce((acc, value) => acc + value, 0);
  }
}
