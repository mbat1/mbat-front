import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackingIncomeTableComponent } from './tracking-income-table.component';

describe('TrackingIncomeTableComponent', () => {
  let component: TrackingIncomeTableComponent;
  let fixture: ComponentFixture<TrackingIncomeTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrackingIncomeTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TrackingIncomeTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
